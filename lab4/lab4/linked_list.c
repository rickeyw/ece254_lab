#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include "linked_list.h"

void insert(node **head, node *node_to_insert) {

    // Empty list
    if (*head == NULL) {
        (*head) = node_to_insert;
        return;
    }

    // Get the location one prior to our target
    node *target_next = *head;
    node *target_prev = NULL;
    while (target_next != NULL) {
        assert(target_next->next != target_next);
        
        // Insert point reached (overshot by 1 node)
        if (target_next > node_to_insert) {
            break;
        }
        
        // If at the tail
        if (target_next->next == NULL) {
            target_next->next = node_to_insert;
            return;
        }

        target_prev = target_next;
        target_next = target_next->next;
    }

    if (target_next == *head) {
        (*head) = node_to_insert;
    }

    // Prevent overwriting assignment before assignment
    assert(target_prev != node_to_insert);
    assert(target_next != node_to_insert);
    
    node_to_insert->next = target_next;
    target_prev->next = node_to_insert;

    assert(node_to_insert->next != node_to_insert);
}

// Initialize a node with default values
void init_node(node *n, size_t size) {
    n->next = NULL;
    n->free = 1;
    n->size = size;
}

// Search for the node that most closely matches the argument, size
node *search_best(node **head, size_t size) {
    node *curr = *head;
    node *best = NULL;
    size_t min_delta_size = SIZE_MAX; // max limit
    while (curr != NULL) {
        assert(curr != curr->next);
        if (curr->free == 1 && curr->size >= size && (curr->size - size) < min_delta_size) {
            min_delta_size = curr->size - size;
            best = curr;
        }
        curr = curr->next;
    }

    return best;
}

// Search for the node with greatest block size
node *search_max(node **head) {
    assert(*head);
    node *curr = *head;
    node *max = *head;
    while (curr != NULL) {
        if (curr->free == 1 && (max->free == 0 || curr->size > max->size)) {
            max = curr;
        }
        curr = curr->next;
    }

    if (max->free == 0) {
        return NULL;
    }

    return max;
}

// Combine free adjacent nodes
void coalesce(node **head) {
    node *curr = *head;
    
    // Ensure linked list is valid
    test_null(head);
    assert(curr->next != curr->next->next);
    
    while (curr && curr->next) {
        
        assert(curr->next != curr->next->next);
        test_null(head);
        
        // If the next block is also free, we combine them
        if (curr->free && curr->next->free && (curr + curr->size) == curr->next) {
            curr->size += curr->next->size;
            curr->next = curr->next->next;
        }
        curr = curr->next;
    }
}

// Test if a linked list is circular
void test_circular(node **head) {
    node *curr = *head;
    while (curr) {
        assert(curr != curr->next);
        curr = curr->next;
    }
}

// Test if a node is null
void test_null(node **head) {
    assert(*head);
    node *curr = *head;
    while (curr) {
        curr = curr->next;
    }
}





