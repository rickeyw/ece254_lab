/**
 * @file memory.c
 * @brief: ECE254 Lab: memory allocation algorithm comparison template file
 * @author: 
 * @date: 2015-11-20
 */

/* includes */
#include <stdio.h> 
#include <stdlib.h> 
#include "mem.h"

/* defines */

/* Globals */
node **bestfit_list;
node **worstfit_list;

/* Functions */

// Slice the node so that the free space becomes a new node
int make_free_block_node(node **list, node *block_alloc, size_t alloc_szreq) {
    
    size_t freeblock_sz = block_alloc->size - alloc_szreq;
    
    // Node has to fit inside block_alloc
    if (freeblock_sz > sizeof(node)) {

        node *free_block = block_alloc + alloc_szreq / sizeof(node);
        init_node(free_block, freeblock_sz);
        
        insert(list, free_block);
        
        return 1;
    }

    return 0;
}

int best_fit_memory_init(size_t size) {

    // Invalid size
    if (size < sizeof(node)) {
        return -1;
    }

    // Allocate the memory into a linked list
    if ((bestfit_list = malloc(size)) == NULL) {
        return -1;
    }

    // Initialize the first element in the list
    *bestfit_list = (node *) (bestfit_list + sizeof(node *));
    init_node(*bestfit_list, size);

    return 0;
}

int worst_fit_memory_init(size_t size) {
    
    // Invalid size
    if (size < sizeof(node)) {
        return -1;
    }

    // Allocate the memory into a linked list
    if ((worstfit_list = malloc(size)) == NULL) {
        return -1;
    }

    // Initialize the first element in the list
    *worstfit_list = (node *) (worstfit_list + sizeof(node *));
    init_node(*worstfit_list, size);

    return 0;
}

/* memory allocators */
void *best_fit_alloc(size_t size_request) {
    
    // Invalid size_request
    if (size_request == 0) {
        return NULL;
    }

    // Round the requested size_request to four bytes alignment
    if ((size_request & 3) > 0) {
        size_request += 4 - (size_request & 3);
    }

    // Get the memory block with the least available free space
    node *bestfit_block = NULL;
    size_t size_required = size_request + sizeof(node);
    
    // Check if there is enough free space, including the overhead
    bestfit_block = search_best(bestfit_list, size_required);
    if (!bestfit_block) return NULL;

    test_circular(bestfit_list);
    
    // If a free block was allocated, we shrink the bestfit block sz
    if (make_free_block_node(bestfit_list, bestfit_block, size_required)) {
        bestfit_block->size = size_required;
    }
    bestfit_block->free = 0;

    test_circular(bestfit_list);

    // Combine free adjacent blocks
    coalesce(bestfit_list);

    // Return pointer is the memory block + sizeof( node )
    void *ptr = bestfit_block + 1;
    return ptr;
}


void *worst_fit_alloc(size_t size_request) {
    
    // Invalid size_request
    if (size_request == 0) {
        return NULL;
    }

    // Round the requested size_request to four bytes alignment
    if ((size_request & 3) > 0) {
        size_request += 4 - (size_request & 3);
    }

    test_null(worstfit_list);

    // Get the memory block with the most free space
    node *worstfit_block;
    size_t size_required = size_request + sizeof(node);
    if (!(worstfit_block = search_max(worstfit_list))) return NULL;
    
    // Check if there is enough free space
    if (worstfit_block->size < size_required) return NULL;

    if(make_free_block_node(worstfit_list, worstfit_block, size_required)){
        worstfit_block->size = size_required;
    }
    worstfit_block->free = 0;

    test_null(worstfit_list);

    // Combine free adjacent blocks
    coalesce(worstfit_list);

    test_null(worstfit_list);

    // Return pointer is the memory block + sizeof( node )
    void *ptr = worstfit_block + 1;
    return ptr;
}

/* memory de-allocator */
void best_fit_dealloc(void *ptr) {
    
    // Account for sizeof( node )
    ptr = ptr - sizeof(node);

    // Iterate until ptr is found
    node *curr = *bestfit_list;
    while (curr != NULL) {
        if (ptr == curr) {
           
            // Set block to free
            curr->free = 1;

            // Combine free adjacent blocks
            coalesce(bestfit_list);

            return;
        }
        curr = curr->next;
    }
    return;
}

void worst_fit_dealloc(void *ptr) {
    test_null(worstfit_list);
    
    // Account for sizeof( node )
    ptr = ptr - sizeof(node);
    
    // Iterate until ptr is found
    node *curr = *worstfit_list;
    while (curr != NULL) {
        if (ptr == curr) {
            
            // Set block to free
            curr->free = 1;

            // Combine free adjacent blocks
            coalesce(worstfit_list);
            return;
        }

        curr = curr->next;
    }
}

/* memory algorithm metric utility function(s) */

/* count how many free blocks are less than the input size */
int best_fit_count_extfrag(size_t size) {
    int count = 0;
    node *curr = *bestfit_list;

    // Iterate through all memory blocks
    while (curr != NULL) {

        // Count free blocks of size less than size
        if (curr->free == 1 && (curr->size - sizeof(node)) < size) {
            count++;
        }

        curr = curr->next;
    }

    return count;
}

int worst_fit_count_extfrag(size_t size) {
    int count = 0;
    node *curr = *worstfit_list;

    coalesce(worstfit_list);

    // Iterate through all memory blocks
    while (curr != NULL) {

        // Count free blocks of size less than size
        if (curr->free == 1 && (curr->size - sizeof(node)) < size) {
            count++;
        }

        curr = curr->next;
    }

    return count;
}
