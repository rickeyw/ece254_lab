/**
  * ECE254 Linux Dynamic Memory Management Lab
  * @file: main_test.c
  * @brief: The main file to write tests of memory allocation algorithms
  */

/* includes */
/* system provided header files. You may add more */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <x86intrin.h>

/* non-system provided header files. 
   Do not include more user-defined header files here
 */
#include "mem.h"

// GLOBALS!
#define BOOK_INIT_END 100
#define BOOK_AUTO_BEGIN 101
#define BOOK_MAX 1000
#define TEST_MEM_SIZE 102400 // initialise the size
#define RAND_NOMINAL 10
#define RAND_RANGE 1
#define RAND_BLKSIZE_MIN RAND_NOMINAL-RAND_RANGE
#define RAND_BLKSIZE_MAX RAND_NOMINAL+RAND_RANGE

unsigned int random_number(unsigned int min_num, unsigned int max_num) {
    unsigned int result = 0, low_num = 0, hi_num = 0;
    if (min_num < max_num) {
        low_num = min_num;
        hi_num = max_num + 1; // this is done to include max_num in output.
    } else{ low_num=max_num; hi_num=min_num+1; }
    result = (rand() % (hi_num - low_num)) + low_num;
    return result;
}

void allocate_both(size_t sizeToAllocate, int index, void **pb, void **pw) {
   // printf("Alloc %3d (i=%2d)\n", sizeToAllocate, index);
    pb[index] = best_fit_alloc(sizeToAllocate);
    pw[index] = worst_fit_alloc(sizeToAllocate);
}

void deallocate_both(int index, void **pb, void **pw) {
   // printf("Dealloc (i=%2d)\n", index);
    best_fit_dealloc(pb[index]);
    worst_fit_dealloc(pw[index]);

    pb[index] = NULL;
    pw[index] = NULL;
}

void count_extfrag_both( unsigned int block_size ) {
    int best_extfrag_count = best_fit_count_extfrag(block_size);
    int worst_extfrag_count = worst_fit_count_extfrag(block_size);
    
    printf("ExtFrag Blocksize = %2d\t Best = %2d\t Worst = %2d\n", block_size, best_extfrag_count, worst_extfrag_count);
}

int main(int argc, char *argv[]) {
    int i=0, idx, j=0;
    void *pb[BOOK_MAX] = {0}, *pw[BOOK_MAX] = {0};  // Accounting book...
    unsigned int size_book[BOOK_MAX] = {0};

    srand((unsigned int) time(NULL));
    best_fit_memory_init(TEST_MEM_SIZE);
    worst_fit_memory_init(TEST_MEM_SIZE);

    /*
     * NUMBERING RANGES (zero-based indexing, i)
     * 0 to BOOK_INIT_END are original allocated blocks.
     * BOOK_INIT_END + 1 to BOOK_AUTO_BEGIN are manual steps
     * BOOK_AUTO_SEQ to BOOK_MAX are randomly allocated blocks.
     */
    
    /*
     * Test 0
     */
    
//    allocate_both( 2 * 1024, 0, pb, pw );
//    allocate_both( 2 * 1024, 1, pb, pw );
//    deallocate_both( 0, pb, pw);
//    allocate_both( 1 * 1024, 0, pb, pw );
//
//    // Best fit = 1 frag. Worst fit = 0 frag
//    printf("Test 0: \n");
//    count_extfrag_both(TEST_MEM_SIZE);
//
//    deallocate_both(0, pb, pw);
//    deallocate_both(1, pb, pw);
//
//    // Make sure there is only one block
//    count_extfrag_both(TEST_MEM_SIZE);
//
//      printf("Test 1: \n");
//    for( i = 0; i < 10; i++ ) {
//        allocate_both( 1024, 0, pb, pw );
//        deallocate_both( 0, pb, pw);
//    }
//
//    // Make sure there is only one block
//
//    count_extfrag_both(TEST_MEM_SIZE);

    /*
     * TEST MAIN
     *  Random simulation of memory access, using predefined variables.
     */

    printf("Test Main: \n");
    // Fill it up with many many blocks of similar size
    for (i = 0; i < BOOK_INIT_END; i++) {
        size_book[i] = random_number(RAND_BLKSIZE_MIN, RAND_BLKSIZE_MAX);
        allocate_both(size_book[i], i, pb, pw);
    }
    count_extfrag_both(TEST_MEM_SIZE);
    // Randomly create segments
    for (j=1; j < BOOK_INIT_END; j += 2){
        idx = random_number(0, BOOK_INIT_END);
        deallocate_both(idx, pb, pw);
    }
    printf("Segment pattern complete, both should have same exfrag:\n");
    count_extfrag_both(TEST_MEM_SIZE);

    i = BOOK_AUTO_BEGIN;
    while (i <= BOOK_MAX) {
        size_book[i] = random_number(RAND_BLKSIZE_MIN, RAND_BLKSIZE_MAX);

        if (random_number(0, 1)) { // 1 in 2 chance to deallocate
            idx = random_number(1, (unsigned int) i);
            if (pw[idx]) {
                deallocate_both(idx, pb, pw);
                size_book[idx] = size_book[i];
                allocate_both(size_book[i], idx, pb, pw);
            }
        }
        // Allocate similar sized block
        allocate_both(size_book[i], i, pb, pw);

        if (random_number(0, 1)) { // 1 in 2 chance to deallocate
            idx = random_number(1, (unsigned int) i);
            if (pw[idx]) {
                deallocate_both(idx, pb, pw);
                size_book[idx] = size_book[i];
                allocate_both(size_book[i], idx, pb, pw);
            }
        }
        ++i;
    }
    printf("Autorun end\n");
    count_extfrag_both(TEST_MEM_SIZE);

    // Edge case - Null Head Tester
//    allocate_both(22, 0, pb, pw);
//    allocate_both(22, 1, pb, pw);
//    deallocate_both(0, pb, pw);
//    allocate_both(18, 3, pb, pw);
//    allocate_both(22, 4, pb, pw);
//    allocate_both(21, 5, pb, pw);
//    deallocate_both(1, pb, pw);
//    allocate_both(18, 1, pb, pw);
//    allocate_both(18, 6, pb, pw);
//    allocate_both(22, 7, pb, pw);
//    allocate_both(19, 8, pb, pw);
//    allocate_both(20, 9, pb, pw);
//    deallocate_both(9, pb, pw);
//    allocate_both(22, 9, pb, pw);
//    allocate_both(22, 10, pb, pw);

    // Edge case - Circular Tester
//    allocate_both(12, 0, pb, pw);
//    allocate_both(12, 2, pb, pw);
//    allocate_both(10, 3, pb, pw);
//    deallocate_both(3, pb, pw);
//    allocate_both(10, 4, pb, pw);
//    allocate_both(10, 5, pb, pw);
//    allocate_both(12, 6, pb, pw);
//    allocate_both(12, 7, pb, pw);
//    deallocate_both(6, pb, pw);
//    allocate_both(8, 8, pb, pw);

    return 0;
}
