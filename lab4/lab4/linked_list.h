/**
 * @file memory.h
 * @brief: ECE254 Lab: memory allocators
 * @author:
 * @date: 2015-11-20
 */

#ifndef LINKED_LIST_H_
#define LINKED_LIST_H_

#include <stddef.h>

typedef struct Node {
    size_t size;
    unsigned short int free; // 1 for free, 0 for not free
    struct Node *next;
} node;

typedef struct List {
    struct Node *head;
} list;

void init_node(node *n, size_t size);
void insert(node **head, node *node_to_insert);

node *search_best(node **head, size_t size);
node* search_max(node **head);
void coalesce( node **head );

void test_circular(node **head);
void test_null(node **head);

#endif // !LINKED_LIST_H_
