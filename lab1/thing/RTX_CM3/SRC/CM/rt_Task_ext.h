/**
* @file: rt_Task_ext.h
*/

extern struct OS_TCB *os_idle_TCB_reference;

extern int rt_tsk_count_get (void);
/* end of file */
