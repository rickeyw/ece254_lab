/**
* @file: rt_Task_ext.c
*/
#include "rt_TypeDef.h"
#include "RTX_Config.h"
#include "rt_System.h"
#include "rt_Task.h"
#include "rt_List.h"
#include "rt_MemBox.h"
#include "rt_Robin.h"
#include "rt_HAL_CM.h"
#include "rt_Task_ext.h"

int rt_tsk_count_get (void) {

	U32 tasks_active = 0;
	P_TCB task_context;
  U32 tid;

	// Same loop as the static OS_TID rt_get_TID (void) to check for tasks in os_active_TCB
  for (tid = 1; tid <= os_maxtaskrun; tid++) {
    if (os_active_TCB[tid-1] != NULL) {
			task_context = os_active_TCB[tid-1];
			if (task_context->state != INACTIVE){
				tasks_active++;
			}
    }
  }
	
	/*
	* In addition to OS_TASKCNT user tasks, the system creates one system task os_idle_demon. 
	* This task is always required by the RTX kernel. 
	* Total number of concurrent running tasks is OS_TASKCNT+1 (number of user tasks plus one system task).
	* 
	*/
//	tid=255;
	if (os_idle_TCB_reference != NULL) {
		//	task_context = os_active_TCB[tid];
			if (os_idle_TCB_reference->state != INACTIVE){
				tasks_active++;
			}
  }
	
// 	if(os_idle_TCB->state != INACTIVE){
// 		tasks_active++;
// 	}

	return tasks_active;
}
/* end of file */
