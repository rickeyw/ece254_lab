#include <fcntl.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mqueue.h>
#include <math.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include "common.h"
#include "consumer.h"

#define _XOPEN_SOURCE 600

bool g_continue = true;

void sig_handler(int sig)
{
    g_continue = false;
}

int run_consumer(int cons_id, int num_consumers, int num_producers, sem_t* producer_done, sem_t* consumer_crit, int* global_count) {
//    sem_t *producer_done, *consumer_crit;
    mqd_t qdes;
    mode_t mode = S_IRUSR | S_IWUSR;
    struct mq_attr attr;
    double result;
    int mq_return = 0;
    int prod_done_count = 0;

    bool dumb = true;
//    producer_done = sem_open(PROD_DONE_SEM, O_RDONLY); /* Open a preexisting semaphore. */
//    consumer_crit = sem_open(CONS_CRIT_SEM, O_RDWR);

    attr.mq_maxmsg  = QUEUE_SIZE;
    attr.mq_msgsize = sizeof(int);
    attr.mq_flags   = O_NONBLOCK;	/* a blocking queue  */

    qdes  = mq_open(QUEUE_NAME, O_RDONLY, mode, &attr);
    if (qdes == -1 ) {
        perror("mq_open()");
        exit(1);
    }

    //printf("Consumer Created: #%d\n", cons_id);

    //while(dumb);

    signal(SIGINT, sig_handler);	/* install Ctl-C signal handler */


    // int num_producers = 10;

    while (g_continue) {
        int incoming;
        struct timespec ts = {time(0) + 1, 0};

        //If all producers are dead, then check if queue is empty. If it's not then keep going.
        //>> Critical Section
        sem_wait(consumer_crit);
        sem_getvalue(producer_done, &prod_done_count);

        // Producers have finished.
        if(prod_done_count == num_producers){
            int returnval;
            returnval = mq_getattr(qdes, &attr);
            if(attr.mq_curmsgs == 0){ // No more messages
                sem_post(consumer_crit);
              return 0;
            }
        }

        if(global_count[0]==global_count[1]){
            sem_post(consumer_crit);
            return 0;
        }

        //while(attr.mq_curmsgs > 0 && ){
            mq_return = mq_receive(qdes,  (char *) &incoming, sizeof(long), 0);
            ++global_count[0];
//            if(mq_return > 0){
//                break;
//            }
//        }

        // chagne this to timed receive, so they die.
       // mq_return = mq_timedreceive(qdes, (char *) &incoming, \
         //                  sizeof(int), 0, &ts);
        sem_post(consumer_crit);
        //>> Critical Section

        if(mq_return < 0){
          fprintf(stderr, "Consumer #%i failed to mq_receive. %s\n", cons_id, strerror(errno));
        } else if (mq_return > 0) {
          #if DEBUG_ENABLE
          printf("Cons #%i received %d\n", cons_id, incoming);
          #endif

          result = sqrt(incoming);
          if(ceilf(result) == result){
            printf("%d\t%d\t%d\n", cons_id, incoming, (int)result);
          }

        }

        /* only block for a limited time if the queue is empty */
        // if (mq_timedreceive(qdes, (char *) &incoming, \
        //                     sizeof(int), 0, &ts) == -1) {
        //     perror("mq_timedreceive() failed");
        //     printf("Type Ctrl-C and wait for 5 seconds to terminate.\n");
        // } else {
        //     printf("Received int %d\n", \
        //            incoming);
        // }
    };


    if (mq_close(qdes) == -1) {
        perror("mq_close() failed");
        exit(2);
    }
//
    return 0;
}
