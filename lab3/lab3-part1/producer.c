#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <semaphore.h>
#include "common.h"
#include "producer.h"

int run_producer(int id, int num_integers, int num_producers, sem_t *producer_done, sem_t *ptr) {
    mqd_t qdes;
   // sem_t *producer_done;
    //producer_done = sem_open(PROD_DONE_SEM, O_RDWR);

    int oflag = O_RDWR | O_CREAT;
    mode_t mode = S_IRUSR | S_IWUSR;
    struct mq_attr attr;

    attr.mq_maxmsg  = QUEUE_SIZE;
    attr.mq_msgsize = sizeof(int);
    attr.mq_flags   = 0;		/* a blocking queue  */

    /*
     * oflag = O_RDWR | O_CREAT
     *  O_RDWR: queue can receive and send messages
     *  O_CREAT: queue is created if it does not exist
     * mode = S_IRUSR | S_IWUSR
     *  S_IRUSR: read permission, owner
     *  S_IWUSR: write permission, owner
     */
    qdes = mq_open(QUEUE_NAME, oflag, mode, &attr);
    if (qdes == -1 ) {
        perror("mq_open() failed");
        return(1);
    }

    int mq_send_status;
    int i;
    for (i = id; i < num_integers; i = i + num_producers) {
        mq_send_status = mq_send(qdes, (char*) &i, sizeof(int), 0);

        if(mq_send_status == -1) {
            perror("mq_send() failed");
        } else {
        //  fprintf(stderr, "Producer #%d mq_send_status is %d\n", id, mq_send_status);
        #if DEBUG_ENABLE
        printf("Prod #%i sent %i\n", id, i);
        #endif
        }
    }

    if (mq_close(qdes) == -1) {
        perror("mq_close() failed");
        return(2);
    }
//
    sem_post(producer_done);
//    sem_close(producer_done);
    return 0;
}
