#!/bin/bash

: '
speedTest.sh
	Based on runSuiteNG --- Next Generation RunSuite


Outputs a comma delimited text file. Each line corresponds to the args-file
	argument, with comma-separated run times as each column.

To use data:
	Import into Excel, then transpose.
	Compute average per column.
	???
	Profit!
'

# Test for arguments
if [ ${#} -ne 3 ]; then
  echo >&2 "Usage: `basename $0` args-file repititions program"
  exit 1
fi

test_suite=$1
reps=$2
sut=$3 #Software Under Test

# Check if things exist
if [ ! -r $test_suite ]; then
    echo >&2 "Test suite file $test_suite cannot be read!"
    exit 2
fi

if [ $(wc -m < $test_suite) == "0" ]; then
    echo >&2 "Test suite file $test_suite is empty!"
    exit 2
fi
# stolen from stackoverflow
command -v $sut >/dev/null 2>&1 || { echo >&2 "Command $sut cannot be run. Is this executable?"; exit 1; }

# Prepare for temp file
#sut_stdout=$(mktemp)

error_count=0

# Read suite.txt line by line
while IFS='' read -r line || [[ -n "$line" ]]; do
    # echo "Read $line"
    #read content of file
    sut_in=$line
    sut_in+=".in"
    if [ ! -r $sut_in ]; then
        echo >&2 "Input file for $line cannot be read or does not exist =("
        exit 2
    fi
    sut_in=$(cat "$sut_in" 2>/dev/null)

    test_out=$line
    test_out+=".out"

    sut_args=$line
    sut_args+=".args"
    if [ ! -r ${sut_args} ]; then
        # No argument file available
        #${sut} < "${line}.in" 1>${sut_stdout} 2> /dev/null
        sut_out=$(cat ${line}.in | ${sut} 2>/dev/null)
        err_code=$?
		else if [ ! -r ${line}.in ]; then
				# No input file available, args only
        sut_args=$(cat ${sut_args} 2>/dev/null)
        #${sut} ${sut_args} < "${line}.in" 1>${sut_stdout} 2> /dev/null
        $(${sut} $(cat ${line}.args))
        err_code=$?
    else
        # Run with argument and std input
        sut_args=$(cat ${sut_args} 2>/dev/null)
        #${sut} ${sut_args} < "${line}.in" 1>${sut_stdout} 2> /dev/null
        sut_out=$(cat ${line}.in | ${sut} $(cat ${line}.args) 2>/dev/null)
        err_code=$?
    fi

    if [ "$sut_out" != "$test_out" ]; then
		((error_count++))
		echo "------------------------------------"
		echo -e "\e[1;91mFailed: \e[0m$line"
		if [ -r "${line}.args" ]; then
			echo "Arguments:"
			echo $sut_args
		fi
        echo "Input:"
        echo $sut_in
        echo "Expected:"
        echo $test_out
        echo "Actual:"
        echo $sut_out
		if [ $err_code -ne 0 ]; then
            echo "Return code: $err_code"
        fi
		echo "------------------------------------"
	else
			echo -e "\e[92mPassed: \e[0m${line}"
    fi
done < "$test_suite"


if [ $error_count -eq 0 ]; then
	echo "------------------------------------"
	echo -e "\e[92mAll tests passed =) \e[0m"
else
	echo "------------------------------------"
	echo "Total failed tests: ${error_count}"
fi

#rm $sut_stdout
