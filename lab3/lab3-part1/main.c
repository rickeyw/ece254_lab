#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <semaphore.h>
#include <wait.h>
#include  <sys/types.h>
#include  <sys/ipc.h>
#include  <sys/shm.h>
#include "consumer.h"
#include "common.h"
#include "producer.h"

#define USE_PROCESS 1

static int *global_count;

int process_main(int num_integers, int buffer_size, int num_producers, int num_consumers, sem_t *producer_done,
                 sem_t *consumer_crit, int *global_count, int *ShmID);
int thread_main( int num_integers, int buffer_size, int num_producers, int num_consumers );

int main(int argc, const char * argv[]) {
    struct timeval tv;
    double t1;
    double t2;
    int num_integers, buffer_size, num_producers, num_consumers;
    sem_t *producer_done, *consumer_crit;

    int temp;

    int ShmID[3]; //Three shared memories
    ShmID[0] = shmget(IPC_PRIVATE, 5*sizeof(int), IPC_CREAT | 0666);
    if (ShmID[0] < 0) {
        fprintf(stderr, "*** shmget error (server) ***\n");
        exit(1);
    }
  //  printf("Server has received a shared memory of FIVE integers...\n");
    global_count = (int *) shmat(ShmID[0], NULL, 0);
    if ((int) global_count == -1) {
        fprintf(stderr, "*** shmat error (server) ***\n");
        exit(1);
    }

    ShmID[2] = shmget(IPC_PRIVATE, sizeof(consumer_crit), IPC_CREAT | 0666);
    consumer_crit = (sem_t *) shmat(ShmID[2], NULL, 0);

    ShmID[1] = shmget(IPC_PRIVATE, sizeof(producer_done), IPC_CREAT | 0666);
    producer_done = (sem_t *) shmat(ShmID[1], NULL, 0);

    // Get the starting time of execution
    gettimeofday(&tv, NULL);
    t1 = tv.tv_sec + tv.tv_usec / 1000000.0;

    // Require 4 arguments (N, B, P, C)
    if (argc != 5) {
        fprintf(stderr, "Usage: ./produce N B P C \n");
        fprintf(stderr, "\t N - Total numer of integer to produce \n");
        fprintf(stderr, "\t B - Buffer size \n");
        fprintf(stderr, "\t P - Number of Producers \n");
        fprintf(stderr, "\t C - Number of Consumers \n");
        return 1;
    }

    sscanf (argv[1],"%d",&num_integers);
    sscanf (argv[2],"%d",&buffer_size);
    sscanf (argv[3],"%d",&num_producers);
    sscanf (argv[4],"%d",&num_consumers);

//    producer_done = sem_open(PROD_DONE_SEM,  O_RDWR | O_CREAT | O_TRUNC, 0644, 0);
//    consumer_crit = sem_open(CONS_CRIT_SEM,  O_RDWR | O_CREAT | O_TRUNC, 0644, 1);

    sem_init(producer_done, 1, 0);
    sem_init(consumer_crit, 1, 1);

    sem_getvalue(producer_done, &temp);
    //  fprintf(stderr, "START producer_done count is now %d\n", temp);
    sem_getvalue(consumer_crit, &temp);
    //  fprintf(stderr, "START consumer_crit count is now %d\n", temp);

    // fprintf(stderr, "Server has attached the shared memory...\n");
    global_count[0] = 0;
    global_count[1] = num_integers;

#if USE_PROCESS
    process_main(num_integers, buffer_size, num_producers, num_consumers, producer_done, consumer_crit, global_count, ShmID);
#else
    thread_main(num_integers, buffer_size, num_producers, num_consumers);
#endif

    // Main returns here.
    // Print the total execution time
    gettimeofday(&tv, NULL);
    t2 = tv.tv_sec + tv.tv_usec / 1000000.0;
    printf("System execution time: %.6lf seconds\n", t2 - t1);

    // Clean up
   sem_destroy(producer_done);
   sem_destroy(consumer_crit);

    return 0;
}

int process_main(int num_integers, int buffer_size, int num_producers, int num_consumers, sem_t *producer_done,
                 sem_t *consumer_crit, int *global_count, int *ShmID) {
    int status = 0;
    pid_t child_PID, wait_ID;
    int i;

    #if DEBUG_ENABLE
    printf("Forking up to num_producers #%d\n", num_producers);
    #endif

    for (i = 0; i < num_producers; i++) {
        #if DEBUG_ENABLE
        printf("FORKING PROD: #%d\n", i);
        #endif
        child_PID = fork();

        if(child_PID < 0) {

            /* Error occurred */
            fprintf(stderr, "FORK FAILED for Producer #%i\n", i);
            return 1;

        } else if(child_PID == 0) {
            int temp = -1;

            /* Child process */
            #if DEBUG_ENABLE
            printf("FORKED run_producer Created: #%d", i);
            #endif
            run_producer(i, num_integers, num_producers, producer_done, consumer_crit);
            #if DEBUG_ENABLE
            sem_getvalue(producer_done, &temp);
            printf("someone died and producer_done count is now %d\n", temp);
            #endif
            //sem_close(producer_done);
            exit(0);
        } else {
            #if DEBUG_ENABLE
            printf("Producer child PID %i\n", child_PID);
            #endif
        }
    }

    #if DEBUG_ENABLE
    printf("Fork up to num_consumers #%d\n", num_consumers);
    #endif

    for (i = 0; i < num_consumers; ++i) {
        child_PID = fork();
        if(child_PID < 0) {

            /* Error occurred */
            fprintf(stderr, "FORK FAILED for Consumer #%i\n", i);
            return 1;

        } else if(child_PID == 0) {
            /* Child process */
            #if DEBUG_ENABLE
            printf("Consumer Created: #%d", i);
            #endif
            run_consumer(i, num_consumers, num_producers, producer_done, consumer_crit, global_count);
            #if DEBUG_ENABLE
            printf("Child ID %d died \n", i);
            #endif
            exit(0);

        } else {
            #if DEBUG_ENABLE
            printf("Consumer Child PID %i\n", child_PID);
            #endif
        }
    }

    /* Parent process */

    /* Wait for child processes to return */
    while ((wait_ID = wait(&status)) > 0) {

    }

  //  fprintf(stderr, "Server has detected the completion of its child...\n");
    shmdt((void *) global_count);
    shmdt((void *) producer_done);
    shmdt((void *) consumer_crit);
  //  fprintf(stderr, "Server has detached its shared memory...\n");
    shmctl(ShmID[0], IPC_RMID, NULL);
    shmctl(ShmID[1], IPC_RMID, NULL);
    shmctl(ShmID[2], IPC_RMID, NULL);
  //  fprintf(stderr, "Server has removed its shared memory...\n");
  //  fprintf(stderr, "Server exits...\n");

    return 0;
}

int thread_main( int num_integers, int buffer_size, int num_producers, int num_consumers) {
    int i;
    for (i = 0; i < num_producers; i++) {

    }

    for (i = 0; i < num_consumers; i++) {
    }

    return 0;
}
