#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>
#include <sys/stat.h>
#include "common.h"

mqd_t init_queue( mode_t mode ) {
    mqd_t qdes;
    
    int oflag = O_RDWR | O_CREAT;
    struct mq_attr attr;
    
    attr.mq_maxmsg  = QUEUE_SIZE;
    attr.mq_msgsize = sizeof(int);
    attr.mq_flags   = 0;		/* a blocking queue  */
    
    /*
     * oflag = O_RDWR | O_CREAT
     *  O_RDWR: queue can receive and send messages
     *  O_CREAT: queue is created if it does not exist
     * mode = S_IRUSR | S_IWUSR
     *  S_IRUSR: read permission, owner
     *  S_IWUSR: write permission, owner
     */
    qdes = mq_open(QUEUE_NAME, oflag, mode, &attr);
    if (qdes == -1 ) {
        perror("mq_open() failed");
        return(1);
    }
    
    return qdes;
}
