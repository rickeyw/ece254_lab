#ifndef PRODUCER_H_
#define PRODUCER_H_

int run_producer(int id, int num_integers, int num_producers, sem_t *producer_done, sem_t *ptr);

#endif /* PRODUCER_H_ */
