#ifndef CONSUMER_H_
#define CONSUMER_H_

int run_consumer(int cons_id, int num_consumers, int num_producers, sem_t *ptr, sem_t *ptr1, int *pInt);

#endif /* CONSUMER_H_ */
