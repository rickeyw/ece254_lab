#ifndef COMMON_H_
#define COMMON_H_

#define QUEUE_NAME  "/mailbox1_giggly"
#define QUEUE_SIZE  6	/* maximum num of messages in a queue */

#define DEBUG_ENABLE 0 //Set to 0 for no debug messages

#define PROD_DONE_SEM "/producer_done_giggly"
#define CONS_CRIT_SEM "/consumer_crit_giggly"


//mqd_t init_queue( mode_t mode );

#endif /* COMMON_H_ */
