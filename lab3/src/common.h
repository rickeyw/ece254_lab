#ifndef COMMON_H_
#define COMMON_H_

#define USE_PROCESS 1       /* 1 to use process and 0 to use thread */
#define QUEUE_NAME  "/mailbox1_giggly" //Unique name to prevent conflict on ecelinux
#define QUEUE_SIZE  6       /* Max num of messages in a queue */

#define DEBUG_ENABLE 0      /* Set to 0 for no debug messages */

#define PROD_DONE_SEM "/producer_done_giggly"
#define CONS_CRIT_SEM "/consumer_crit_giggly"

struct arg_struct {
    int tid;
    int num_integers;
    int buffer_size;
    int num_producers;
    int num_consumers;
};

#endif /* COMMON_H_ */
