#include <mqueue.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "common.h"
#include "producer.h"



/* Process Producer */

int run_producer(int id, int num_integers, int num_producers, sem_t *producer_done, sem_t *ptr) {
    mqd_t qdes;
    int oflag = O_RDWR | O_CREAT;
    mode_t mode = S_IRUSR | S_IWUSR;
    struct mq_attr attr;

    attr.mq_maxmsg  = QUEUE_SIZE;
    attr.mq_msgsize = sizeof(int);
    attr.mq_flags   = 0;		/* Blocking queue  */

    qdes = mq_open(QUEUE_NAME, oflag, mode, &attr);
    if (qdes == -1 ) {
        perror("mq_open() failed");
        return(1);
    }

    int mq_send_status;
    int i;
    for (i = id; i < num_integers; i = i + num_producers) {
        mq_send_status = mq_send(qdes, (char*) &i, sizeof(int), 0);

        if (mq_send_status == -1) {
            perror("mq_send() failed");
        } else {
            #if DEBUG_ENABLE
                printf("Prod #%i sent %i\n", id, i);
            #endif
        }
    }

    if (mq_close(qdes) == -1) {
        perror("mq_close() failed");
        return(2);
    }

    // Signal that a producer has exited. 
    sem_post(producer_done);

    return 0;
}



/* Thread Producer */

extern int *produced_nums;
extern sem_t produced_length;
extern sem_t produced_free;
extern int start_index;
extern int end_index;
extern pthread_mutex_t consumers_lock;
extern pthread_mutex_t producers_lock;
extern sem_t produced_count;
extern sem_t kill_signal;
extern mode_t mode;

void *thread_producer( void *arg ) {
    struct arg_struct *arg_s = (struct arg_struct*) arg;

    int i = arg_s->tid;
    while( i < arg_s->num_integers ) {
        /* Decrement count of empty spaces in array */
        if( sem_wait(&produced_free) != 0 ) {
            perror("sem_wait() failed");
            break;
        }

        if( pthread_mutex_lock(&producers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        } {

            /* Put a number in the buffer */
            produced_nums[end_index] = i;
            end_index = (end_index + 1) % arg_s->buffer_size;

        } if( pthread_mutex_unlock(&producers_lock) != 0 ) {
            perror("pthread_mutex_unlock() failed");
            break;
        }

        /* Increment count of empty spaces in array */
        if( sem_post(&produced_length) != 0 ) {
            perror("sem_wait() failed");
            break;
        }

        i = i + arg_s->num_producers;
    }

    free(arg);
    pthread_exit(0);
}
