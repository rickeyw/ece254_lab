#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <mqueue.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>
#include "common.h"
#include "consumer.h"

#define _XOPEN_SOURCE 600
bool g_continue = true;

/* Handle Ctrl-C signals */
void sig_handler(int sig)
{
    g_continue = false;
}

/* Process Consumer */
int run_consumer(int cons_id, int num_consumers, int num_producers, sem_t* producer_done, sem_t* consumer_crit, int* global_count) {
    mqd_t qdes;
    mode_t mode = S_IRUSR | S_IWUSR;
    struct mq_attr attr;
    double result;
    int mq_return = 0;
    int prod_done_count = 0;

    attr.mq_maxmsg  = QUEUE_SIZE;
    attr.mq_msgsize = sizeof(int);
    attr.mq_flags   = O_NONBLOCK;	/* Blocking queue  */

    qdes  = mq_open(QUEUE_NAME, O_RDONLY, mode, &attr);
    if (qdes == -1 ) {
        perror("mq_open()");
        exit(1);
    }

    /* Ctrl-C signal handler */
    signal(SIGINT, sig_handler);

    while (g_continue) {
        int incoming;
        struct timespec ts = {time(0) + 1, 0};

        /*
         * If all producers are dead, then check if queue is empty. If it's not then keep going.
         * >> Critical Section
         */
        sem_wait(consumer_crit);
        if (sem_getvalue(producer_done, &prod_done_count) != 0) {
            /* Leave critical section */
            perror("sem_getvalue(producer_done, &prod_done_count) FAIL");
            sem_post(consumer_crit);

            continue;
        }

        /* Producers have finished */
        if(prod_done_count == num_producers){
            int returnval;
            returnval = mq_getattr(qdes, &attr);
            if(attr.mq_curmsgs == 0){ // No more messages
                sem_post(consumer_crit);
                return 0;
            }
        }

        if(global_count[0]==global_count[1]){
            sem_post(consumer_crit);
            return 0;
        }

        mq_return = mq_receive(qdes,  (char *) &incoming, sizeof(long), 0);
        ++global_count[0];

        sem_post(consumer_crit);
        /* >> Critical Section */

        if(mq_return < 0){
            fprintf(stderr, "Consumer #%i failed to mq_receive. %s\n", cons_id, strerror(errno));
        } else if (mq_return > 0) {
            #if DEBUG_ENABLE
                printf("Cons #%i received %d\n", cons_id, incoming);
            #endif

            result = sqrt(incoming);
            if(ceilf(result) == result){
                printf("%d\t%d\t%d\n", cons_id, incoming, (int)result);
            }

        }
    };

    if (mq_close(qdes) == -1) {
        perror("mq_close() failed");
        exit(2);
    }

    return 0;
}



/* Thread Consumer */

extern int *produced_nums;
extern sem_t produced_length;
extern sem_t produced_free;
extern sem_t consumers_waiting;
extern int start_index;
extern int end_index;
extern pthread_mutex_t consumers_lock;
extern pthread_mutex_t producers_lock;
extern sem_t produced_count;
extern sem_t kill_signal;
extern mode_t mode;

void *thread_consumer( void *arg ) {
    struct arg_struct *arg_s = (struct arg_struct*) arg;

    int received;
    int k_signal = 0;

    while(k_signal == 0) {

        if( pthread_mutex_lock(&consumers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        } {

            /* Get count of numbers received */
            int p_count;
            if(sem_getvalue(&produced_count, &p_count) != 0) {
                perror("sem_getvalue(producers_done) failed");
                break;
            }

            /* Get count of numbers being processed */
            int c_wait;
            if(sem_getvalue(&consumers_waiting, &c_wait) != 0) {
                perror("sem_getvalue(consumers_waiting) failed");
                break;
            }

            /* Terminate if all numbers have been received or are being processed */
            if(p_count >= arg_s->num_integers - c_wait) {
                if( pthread_mutex_unlock(&consumers_lock) != 0 ) {
                    perror("pthread_mutex_lock() failed");
                    break;
                }

                free(arg);
                pthread_exit(0);
            }

            /* Increment count of numbers being processed */
            if( sem_post(&consumers_waiting) != 0 ) {
                perror("sem_wait() failed");
                break;
            }

        } if( pthread_mutex_unlock(&consumers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        }

        /* Decrement number of items in array */
        if( sem_wait(&produced_length) != 0 ) {
            perror("sem_wait() failed");
            break;
        }

        if( pthread_mutex_lock(&producers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        } {

            /* Process number */
            received = produced_nums[start_index];
            start_index = (start_index + 1) % arg_s->buffer_size;

        } if( pthread_mutex_unlock(&producers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        }

        /* Increment number of free items in array */
        if( sem_post(&produced_free) != 0 ) {
            perror("sem_post() failed");
            break;
        }

        if( sem_wait(&consumers_waiting) != 0 ) {
            perror("sem_wait() failed");
            break;
        }

        // Increment count of numbers produced
        if( sem_post(&produced_count) != 0 ) {
            perror("sem_wait() failed");
            break;
        }

        /* Determine if square root result is a whole number */
        double result = sqrt(received);
        if(ceilf(result) == result) {
            printf("%d\t%d\t%f\n", arg_s->tid, received, result);
        }

        /* Check for kill signal */
        if(sem_getvalue(&kill_signal, &k_signal) != 0 ) {
            perror("sem_getvalue() failed");
            break;
        }
    }

    free(arg);
    pthread_exit(0);
}
