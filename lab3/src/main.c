#include <fcntl.h>           /* For O_* constants */
#include <mqueue.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>        /* For mode constants */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include "consumer.h"
#include "common.h"
#include "producer.h"


int process_main( int num_integers, int buffer_size, int num_producers, int num_consumers );
int thread_main( int num_integers, int buffer_size, int num_producers, int num_consumers );

int *produced_nums;
sem_t produced_length;
sem_t produced_free;
sem_t consumers_waiting;
int start_index;
int end_index;
pthread_mutex_t consumers_lock;
pthread_mutex_t producers_lock;
sem_t produced_count;
sem_t kill_signal;
const mode_t mode = S_IRUSR | S_IWUSR;

static int *global_count;

/* Program Entry Point */
int main(int argc, const char * argv[]) {
    struct timeval tv;
    double t1;
    double t2;
    int num_integers, buffer_size, num_producers, num_consumers;

    // Get the starting time of execution
    gettimeofday(&tv, NULL);
    t1 = tv.tv_sec + tv.tv_usec / 1000000.0;

    // Require 4 arguments (N, B, P, C)
    if (argc != 5) {
        fprintf(stderr, "Usage: ./produce N B P C \n");
        fprintf(stderr, "\t N - Total numer of integer to produce \n");
        fprintf(stderr, "\t B - Buffer size \n");
        fprintf(stderr, "\t P - Number of Producers \n");
        fprintf(stderr, "\t C - Number of Consumers \n");
        return 1;
    }

    sscanf (argv[1],"%d",&num_integers);
    sscanf (argv[2],"%d",&buffer_size);
    sscanf (argv[3],"%d",&num_producers);
    sscanf (argv[4],"%d",&num_consumers);

    #if USE_PROCESS
        process_main(num_integers, buffer_size, num_producers, num_consumers);
    #else
        thread_main(num_integers, buffer_size, num_producers, num_consumers);
    #endif

    // Print the total execution time
    gettimeofday(&tv, NULL);
    t2 = tv.tv_sec + tv.tv_usec / 1000000.0;
    printf("System execution time: %.6lf seconds\n", t2 - t1);

    return 0;
}


/* Process main method */
int process_main(int num_integers, int buffer_size, int num_producers, int num_consumers) {

    int status = 0;
    pid_t child_PID, wait_ID;
    int i;
    sem_t *producer_done, *consumer_crit;

    int ShmID[3];
    ShmID[0] = shmget(IPC_PRIVATE, 5*sizeof(int), IPC_CREAT | 0666);
    if (ShmID[0] < 0) {
        fprintf(stderr, "*** shmget error (server) ***\n");
        exit(1);
    }

    /* Receive 5 integers of shared memory */
    global_count = (int *) shmat(ShmID[0], NULL, 0);
    if ((int) global_count == -1) {
        fprintf(stderr, "*** shmat error (server) ***\n");
        exit(1);
    }

    ShmID[2] = shmget(IPC_PRIVATE, sizeof(consumer_crit), IPC_CREAT | 0666);
    consumer_crit = (sem_t *) shmat(ShmID[2], NULL, 0);

    ShmID[1] = shmget(IPC_PRIVATE, sizeof(producer_done), IPC_CREAT | 0666);
    producer_done = (sem_t *) shmat(ShmID[1], NULL, 0);

    sem_init(producer_done, 1, 0);
    sem_init(consumer_crit, 1, 1);

    global_count[0] = 0;
    global_count[1] = num_integers;

    #if DEBUG_ENABLE
        printf("Forking up to num_producers #%d\n", num_producers);
    #endif

    for (i = 0; i < num_producers; i++) {
        #if DEBUG_ENABLE
            printf("FORKING PROD: #%d\n", i);
        #endif
        child_PID = fork();

        if(child_PID < 0) {

            /* Error occurred */
            fprintf(stderr, "FORK FAILED for Producer #%i\n", i);
            return 1;

        } else if(child_PID == 0) {
            int temp = -1;

            /* Child process */
            #if DEBUG_ENABLE
                printf("FORKED run_producer Created: #%d", i);
            #endif

            run_producer(i, num_integers, num_producers, producer_done, consumer_crit);

            #if DEBUG_ENABLE
                sem_getvalue(producer_done, &temp);
                printf("someone died and producer_done count is now %d\n", temp);
            #endif

            exit(0);
        } else {
            #if DEBUG_ENABLE
                printf("Producer child PID %i\n", child_PID);
            #endif
        }
    }

    #if DEBUG_ENABLE
        printf("Fork up to num_consumers #%d\n", num_consumers);
    #endif

    for (i = 0; i < num_consumers; ++i) {
        child_PID = fork();

        if(child_PID < 0) {

            /* Error occurred */
            fprintf(stderr, "FORK FAILED for Consumer #%i\n", i);
            return 1;

        } else if(child_PID == 0) {
            /* Child process */
            #if DEBUG_ENABLE
                printf("Consumer Created: #%d", i);
            #endif

            run_consumer(i, num_consumers, num_producers, producer_done, consumer_crit, global_count);

            #if DEBUG_ENABLE
                printf("Child ID %d died \n", i);
            #endif

            exit(0);

        } else {
            #if DEBUG_ENABLE
                printf("Consumer Child PID %i\n", child_PID);
            #endif
        }
    }

    /* Parent process */

    /* Wait for child processes to return */
    while ((wait_ID = wait(&status)) > 0);

    /* Child processes are completed */
    shmdt((void *) global_count);
    shmdt((void *) producer_done);
    shmdt((void *) consumer_crit);

    /* Detach shared memory */
    shmctl(ShmID[0], IPC_RMID, NULL);
    shmctl(ShmID[1], IPC_RMID, NULL);
    shmctl(ShmID[2], IPC_RMID, NULL);

    // Clean up
    sem_destroy(producer_done);
    sem_destroy(consumer_crit);

    return 0;
}



/* Thread main method */

void thread_sig_handler(int sig)
{
    sem_post(&kill_signal);
}

int thread_main( int num_integers, int buffer_size, int num_producers, int num_consumers ) {
    /* Initialize semaphores and mutexes */
    sem_init(&kill_signal, 1, 0);
    sem_init(&produced_count, 1, 0);
    sem_init(&produced_length, 1, 0);
    sem_init(&produced_free, 1, buffer_size);
    sem_init(&consumers_waiting, 1, 0);
    pthread_mutex_init(&consumers_lock, NULL);
    pthread_mutex_init(&producers_lock, NULL);

    /* Initialize circular buffer */
    produced_nums = malloc(buffer_size * sizeof(int));
    start_index = 0;
    end_index = 0;

    /* Ctl-C signal handler */
    signal(SIGINT, thread_sig_handler);

    /* Threads are saved in arrays */
    pthread_t *tid_producers;
    pthread_t *tid_consumers;
    tid_producers = malloc( num_producers * sizeof( pthread_t ) );
    tid_consumers = malloc( num_consumers * sizeof( pthread_t ) );
    pthread_attr_t attr;

    /* Get the default attributes */
    pthread_attr_init(&attr);

    /* Make pthreads joinable */
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    int i;

    /* Create producer threads */
    for (i = 0; i < num_producers; i++) {
        struct arg_struct *arg_s;
        arg_s = malloc( sizeof( struct arg_struct ) );
        arg_s->num_integers = num_integers;
        arg_s->buffer_size = buffer_size;
        arg_s->num_producers = num_producers;
        arg_s->num_consumers = num_consumers;
        arg_s->tid = i;

        pthread_create( &tid_producers[i], &attr, thread_producer, arg_s );
    }

    /* Create consumer threads */
    for (i = 0; i < num_consumers; i++) {
        struct arg_struct *arg_s;
        arg_s = malloc( sizeof( struct arg_struct ) );
        arg_s->num_integers = num_integers;
        arg_s->buffer_size = buffer_size;
        arg_s->num_producers = num_producers;
        arg_s->num_consumers = num_consumers;
        arg_s->tid = i;

        pthread_create( &tid_consumers[i], &attr, thread_consumer, arg_s );
    }

    pthread_attr_destroy(&attr);

    /* Wait for producers to finish */
    for (i = 0; i < num_producers; i++) {
        if( pthread_join(tid_producers[i], NULL) != 0 ) {
            perror("pthread_join() error");
        }
    }

    /* Wait for consumers to finish */
    for (i = 0; i < num_consumers; i++) {
        if( pthread_join(tid_consumers[i], NULL) != 0 ) {
            perror("pthread_join() error");
        }
    }

    /* Cleanup */
    free( produced_nums );
    free( tid_producers );
    free( tid_consumers );

    sem_destroy(&produced_free);
    sem_destroy(&produced_count);
    sem_destroy(&kill_signal);
    sem_destroy(&produced_length);
    sem_destroy(&consumers_waiting);

    pthread_mutex_destroy(&consumers_lock);
    pthread_mutex_destroy(&producers_lock);

    return 0;
}
