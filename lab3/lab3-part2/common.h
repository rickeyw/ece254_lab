#ifndef COMMON_H_
#define COMMON_H_

#define USE_PROCESS 0
#define QUEUE_NAME  "/mailbox1"
#define QUEUE_SIZE  6	/* maximum num of messages in a queue */

#define DEBUG_ENABLE 0 //Set to 0 for no debug messages

struct arg_struct {
    int tid;
    int num_integers;
    int buffer_size;
    int num_producers;
    int num_consumers;
};

#endif /* COMMON_H_ */
