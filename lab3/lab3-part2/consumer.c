#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mqueue.h>
#include <pthread.h>
#include <math.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>
#include <semaphore.h>
#include "common.h"
#include "consumer.h"

#define _XOPEN_SOURCE 600

extern int *produced_nums;
extern sem_t produced_length;
extern sem_t produced_free;
extern sem_t consumers_waiting;
extern int start_index;
extern int end_index;
extern pthread_mutex_t consumers_lock;
extern pthread_mutex_t producers_lock;
extern sem_t produced_count;
extern sem_t kill_signal;
bool g_continue = true;
extern mode_t mode;

void sig_handler(int sig)
{
    g_continue = false;
}

int run_consumer(int cons_id, int num_consumers, int num_producers, sem_t* producer_done, sem_t* consumer_crit, int* global_count) {
    return 0;
}

void *thread_consumer( void *arg ) {
    struct arg_struct *arg_s = (struct arg_struct*) arg;
    
    int received;
    int k_signal = 0;

    while(k_signal == 0) {
        
        printf("Consumer %d past consumers_lock\n", arg_s->tid);
        
        if( pthread_mutex_lock(&consumers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        } {
            
            /* Get count of numbers received */
            int p_count;
            if(sem_getvalue(&produced_count, &p_count) != 0) {
                perror("sem_getvalue(producers_done) failed");
                break;
            }
            
            /* Get count of numbers being processed */
            int c_wait;
            if(sem_getvalue(&consumers_waiting, &c_wait) != 0) {
                perror("sem_getvalue(consumers_waiting) failed");
                break;
            }
            
            /* Terminate if all numbers have been received or are being processed */
            if(p_count >= arg_s->num_integers - c_wait) {
                if( pthread_mutex_unlock(&consumers_lock) != 0 ) {
                    perror("pthread_mutex_lock() failed");
                    break;
                }

                free(arg);
                pthread_exit(0);
            }
            
            /* Increment count of numbers being processed */
            if( sem_post(&consumers_waiting) != 0 ) {
                perror("sem_wait() failed");
                break;
            }

        } if( pthread_mutex_unlock(&consumers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        }

        /* Decrement number of items in array */
        if( sem_wait(&produced_length) != 0 ) {
            perror("sem_wait() failed");
            break;
        }

        if( pthread_mutex_lock(&producers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        } {
            
            /* Process number */
            received = produced_nums[start_index];
            start_index = (start_index + 1) % arg_s->buffer_size;
            
        } if( pthread_mutex_unlock(&producers_lock) != 0 ) {
            perror("pthread_mutex_lock() failed");
            break;
        }
        
        /* Increment number of free items in array */
        if( sem_post(&produced_free) != 0 ) {
            perror("sem_post() failed");
            break;
        }
        
        if( sem_wait(&consumers_waiting) != 0 ) {
            perror("sem_wait() failed");
            break;
        }
        
        // Increment count of numbers produced
        if( sem_post(&produced_count) != 0 ) {
            perror("sem_wait() failed");
            break;
        }
        
        /* Determine if square root result is a whole number */
        double result = sqrt(received);
        if(ceilf(result) == result) {
            printf("%d\t%d\t%f\n", arg_s->tid, received, result);
        }
        
        /* Check for kill signal */
        if(sem_getvalue(&kill_signal, &k_signal) != 0 ) {
            perror("sem_getvalue() failed");
            break;
        }
    }
    
    free(arg);
    pthread_exit(0);
}
