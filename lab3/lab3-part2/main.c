#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <semaphore.h>
#include <wait.h>
#include  <sys/ipc.h>
#include  <sys/shm.h>
#include <pthread.h>
#include "consumer.h"
#include "common.h"
#include "producer.h"

int process_main(int num_integers, int buffer_size, int num_producers, int num_consumers, sem_t *producer_done,
                 sem_t *consumer_crit, int *global_count, int *ShmID);
int thread_main( int num_integers, int buffer_size, int num_producers, int num_consumers );
void thread_sig_handler(int sig);

sem_t produced_length;
sem_t produced_free;
sem_t consumers_waiting;
sem_t produced_count;
sem_t kill_signal;
pthread_mutex_t consumers_lock;
pthread_mutex_t producers_lock;
int *produced_nums;
int start_index;
int end_index;
static int *global_count;
const mode_t mode = S_IRUSR | S_IWUSR;

int main(int argc, const char * argv[]) {
    struct timeval tv;
    double t1;
    double t2;
    int num_integers, buffer_size, num_producers, num_consumers;

    int temp;

    // Get the starting time of execution
    gettimeofday(&tv, NULL);
    t1 = tv.tv_sec + tv.tv_usec / 1000000.0;

    // Require 4 arguments (N, B, P, C)
    if (argc != 5) {
        fprintf(stderr, "Usage: ./produce N B P C \n");
        fprintf(stderr, "\t N - Total numer of integer to produce \n");
        fprintf(stderr, "\t B - Buffer size \n");
        fprintf(stderr, "\t P - Number of Producers \n");
        fprintf(stderr, "\t C - Number of Consumers \n");
        return 1;
    }

    sscanf (argv[1],"%d",&num_integers);
    sscanf (argv[2],"%d",&buffer_size);
    sscanf (argv[3],"%d",&num_producers);
    sscanf (argv[4],"%d",&num_consumers);

#if USE_PROCESS
#else
    thread_main(num_integers, buffer_size, num_producers, num_consumers);
#endif

    // Main returns here.
    // Print the total execution time
    gettimeofday(&tv, NULL);
    t2 = tv.tv_sec + tv.tv_usec / 1000000.0;
    printf("System execution time: %.6lf seconds\n", t2 - t1);

    return 0;
}

int process_main(int num_integers, int buffer_size, int num_producers, int num_consumers, sem_t *producer_done,
                 sem_t *consumer_crit, int *global_count, int *ShmID) {
    return 0;
}

int thread_main( int num_integers, int buffer_size, int num_producers, int num_consumers ) {
    /* Initialize semaphores and mutexes */
    sem_init(&kill_signal, 1, 0);
    sem_init(&produced_count, 1, 0);
    sem_init(&produced_length, 1, 0);
    sem_init(&produced_free, 1, buffer_size);
    sem_init(&consumers_waiting, 1, 0);
    pthread_mutex_init(&consumers_lock, NULL);
    pthread_mutex_init(&producers_lock, NULL);
    
    /* Initialize circular buffer */
    produced_nums = malloc(buffer_size * sizeof(int));
    start_index = 0;
    end_index = 0;
    
    /* Ctl-C signal handler */
    signal(SIGINT, thread_sig_handler);
    
    /* Threads are saved in arrays */
    pthread_t *tid_producers;
    pthread_t *tid_consumers;
    tid_producers = malloc( num_producers * sizeof( pthread_t ) );
    tid_consumers = malloc( num_consumers * sizeof( pthread_t ) );
    pthread_attr_t attr;

    /* Get the default attributes */
    pthread_attr_init(&attr);
    
    /* Make pthreads joinable */
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    int i;
    
    /* Create producer threads */
    for (i = 0; i < num_producers; i++) {
        struct arg_struct *arg_s;
        arg_s = malloc( sizeof( struct arg_struct ) );
        arg_s->num_integers = num_integers;
        arg_s->buffer_size = buffer_size;
    	arg_s->num_producers = num_producers;
        arg_s->num_consumers = num_consumers;
        arg_s->tid = i;
        
        pthread_create( &tid_producers[i], &attr, thread_producer, arg_s );
    }

    /* Create consumer threads */
    for (i = 0; i < num_consumers; i++) {
        struct arg_struct *arg_s;
        arg_s = malloc( sizeof( struct arg_struct ) );
        arg_s->num_integers = num_integers;
        arg_s->buffer_size = buffer_size;
        arg_s->num_producers = num_producers;
        arg_s->num_consumers = num_consumers;
        arg_s->tid = i;
        
        pthread_create( &tid_consumers[i], &attr, thread_consumer, arg_s );
    }
    
    pthread_attr_destroy(&attr);
    
    /* Wait for producers to finish */
    for (i = 0; i < num_producers; i++) {
        if( pthread_join(tid_producers[i], NULL) != 0 ) {
            perror("pthread_join() error");
        }
    }
    
    /* Wait for consumers to finish */
    for (i = 0; i < num_consumers; i++) {
        if( pthread_join(tid_consumers[i], NULL) != 0 ) {
            perror("pthread_join() error");
        }
    }
    
    /* Cleanup */
    free( produced_nums );
    free( tid_producers );
    free( tid_consumers );
    
    sem_destroy(&produced_free);
    sem_destroy(&produced_count);
    sem_destroy(&kill_signal);
    sem_destroy(&produced_length);
    sem_destroy(&consumers_waiting);

    pthread_mutex_destroy(&consumers_lock);
    pthread_mutex_destroy(&producers_lock);

    return 0;
}

void thread_sig_handler(int sig)
{
    sem_post(&kill_signal);
}
