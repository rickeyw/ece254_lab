/**
 * @brief: ECE254 Keil ARM RL-RTX Task Management Lab starter file that calls os_tsk_get()
 * @file: main_task_exp.c
 * @date: 2015/09/13
 */
/* NOTE: This release is for students to use */

#include <LPC17xx.h>
#include "uart_polling.h"
#include <RTL.h>
#include "../../RTX_CM3/INC/RTL_ext.h" /* extended interface header file */
#include <stdio.h>
#include <string.h>

#define NUM_FNAMES 10

extern void os_idle_demon(void);
__task void task1(void);
__task void taskT1(void);
__task void taskT2(void);
__task void taskT3(void);
__task void init (void);
char *state2str(unsigned char state, char *str);
char *fp2name(void (*p)(), char *str);

OS_MUT g_mut_uart;
OS_TID g_tid = 255;

char g_str[16];
char g_tsk_name[16];

struct func_info {
  void (*p)();      // Function pointer
  char name[16];    // Name of the function
};

// Map task functions to readable names
struct func_info g_task_map[NUM_FNAMES] =
{
  // os_idle_demon function ptr to be initialized in main
  { NULL,  "os_idle_demon" },
  { task1, "StatChk" },
  { taskT1, "taskT1" },
	{ taskT2, "taskT2" },
  { taskT3, "TaskT3" },
  { init,  "init" }
};


// Make our memory box with 1 block and 20 bytes in each block
_declare_box (memory_box, 20, 1);

int main(void)
{
	_init_box (memory_box, sizeof (memory_box), 20);
	
	SystemInit();         /* initialize the LPC17xx MCU */
	uart0_init();         /* initilize the first UART */

	/* fill the fname map with os_idle_demon entry point */
	g_task_map[0].p = os_idle_demon;

	printf("Calling os_sys_init()...\n");
	os_sys_init(init);    /* initilize the OS and start the first task */
}

/*--------------------------- init ------------------------------------
 * initialize system resources and create other tasks                  
 *---------------------------------------------------------------------
 */
__task void init(void)
{
	os_mut_init(&g_mut_uart);

	os_mut_wait(g_mut_uart, 0xFFFF);
	printf("init: TID = %d\n", os_tsk_self());
	os_mut_release(g_mut_uart);

	g_tid = os_tsk_create(task1, 1); // task2 with priority 1
	os_mut_wait(g_mut_uart, 0xFFFF);
	printf("init: Created task2 with TID %d\n", g_tid);
	os_mut_release(g_mut_uart);
	
	// Create memory-dependent tasks with varying priorities
	os_tsk_create(taskT1, 1);
	os_tsk_create(taskT2, 5);
	os_tsk_create(taskT3, 2);
	
	// Task must delete itself before exiting
	os_tsk_delete_self();
}

/*
 * ==================== TASKS ====================
 */

/*
 * --------------------------- task2 -----------------------------------
 * Checking states of all tasks in the system                          
 * A task periodically prints task status of each task in the system.
 *---------------------------------------------------------------------
 */
__task void task1(void)
{
	U8 i=1;
	RL_TASK_INFO task_info;

	os_mut_wait(g_mut_uart, 0xFFFF);
	printf("TID\tNAME\t\tPRIO\tSTATE   \t%%STACK\n");
	os_mut_release(g_mut_uart);
	
	for (;;) {
		for(i = 0; i < 10; i++) { // this is a lazy way of doing loop.
			if (os_tsk_get(i+1, &task_info) == OS_R_OK) {
				os_mut_wait(g_mut_uart, 0xFFFF);
				printf("%d\t%s\t\t%d\t%s\t%d%%\n", \
							 task_info.task_id, \
							 fp2name(task_info.ptask, g_tsk_name), \
							 task_info.prio, \
							 state2str(task_info.state, g_str),  \
							 task_info.stack_usage);
				os_mut_release(g_mut_uart);
			}
		}

		if (os_tsk_get(0xFF, &task_info) == OS_R_OK) {
			os_mut_wait(g_mut_uart, 0xFFFF);
			printf("%d\t%s\t\t%d\t%s\t%d%%\n", \
						 task_info.task_id, \
						 fp2name(task_info.ptask, g_tsk_name), \
						 task_info.prio, \
						 state2str(task_info.state, g_str),  \
						 task_info.stack_usage);
			os_mut_release(g_mut_uart);
		}
		
		os_dly_wait (1);
	}
}

/*
 * --------------------------- taskT1 -----------------------------------
 * The generic task that does the following:
 *    A task can allocate a fixed size of memory.
 *    A task will get blocked if there is no memory available when os_mem_alloc() is
 *      called.
 *    A blocked on memory task will be resumed once enough memory is available in the
 *      system.
 * ---------------------------------------------------------------------
 */
__task void taskT1(void)
{
	int i = 0;
  void* temp_mem;
	
	// Ask for 1 block of memory
	os_mut_wait(g_mut_uart, 0xFFFF);
  printf("taskT1: Ask for memory\n");
	os_mut_release(g_mut_uart);
  temp_mem = os_mem_alloc(&memory_box);
	os_mut_wait(g_mut_uart, 0xFFFF);
  printf("taskT1: Received memory\n");
	os_mut_release(g_mut_uart);
	
	// Loop 10 times
	while( i <  10 ) {
		i++;
		os_mut_wait(g_mut_uart, 0xFFFF);
		printf("taskT1: Looping %d\n", i);
		os_mut_release(g_mut_uart);
		os_dly_wait (1);
	}
	
	// Free memory
	os_mem_free( &memory_box, temp_mem );
}

/*
 * --------------------------- taskT2 -----------------------------------
 * The generic task that copy paste from T1
 * ---------------------------------------------------------------------
 */
__task void taskT2(void)
{
	int i = 0;
  void* temp_mem;
	
	// Ask for 1 block of memory
  os_mut_wait(g_mut_uart, 0xFFFF);
  printf("taskT2: Ask for memory\n");
	os_mut_release(g_mut_uart);
  temp_mem = os_mem_alloc(&memory_box);
	os_mut_wait(g_mut_uart, 0xFFFF);
  printf("taskT2: Received memory\n");
	os_mut_release(g_mut_uart);
	
	// Loop 10 times
	while( i <  10 ) {
		i++;
		os_mut_wait(g_mut_uart, 0xFFFF);
		printf("taskT2: Looping %d\n", i);
		os_mut_release(g_mut_uart);
		os_dly_wait (1);
	}
	
	// Free memory
	os_mut_wait(g_mut_uart, 0xFFFF);
	if( os_mem_free( &memory_box, temp_mem ) == OS_R_OK ) {
		printf("taskT2: Freed memory\n");
	} else {
		printf("taskT2: Failed to free memory\n");
	}
	os_mut_release(g_mut_uart);
}

/*
 * --------------------------- taskT3 -----------------------------------
 * The generic task that copy paste from T1
 *---------------------------------------------------------------------
 */
__task void taskT3(void)
{
	int i = 0;
	void* temp_mem;
	
	// Ask for 1 block of memory
  os_mut_wait(g_mut_uart, 0xFFFF);
  printf("taskT3: Ask for memory\n");
	os_mut_release(g_mut_uart);
  temp_mem = os_mem_alloc(&memory_box);
	os_mut_wait(g_mut_uart, 0xFFFF);
  printf("taskT3: Received memory\n");
	os_mut_release(g_mut_uart);
	
	// Loop 10 times
	while( i <  10 ) {
		i++;
		os_mut_wait(g_mut_uart, 0xFFFF);
		printf("taskT3: Looping %d\n", i);
		os_mut_release(g_mut_uart);
		os_dly_wait (1);
	}
	
	// Free memory
	os_mem_free( &memory_box, temp_mem );
}

/**
 * @brief: convert state numerical value to string represenation
 * @param: state numerical value (macro) of the task state
 * @param: str   buffer to save the string representation,
 *               buffer to be allocated by the caller
 * @return:the string starting address
 */
char *state2str(unsigned char state, char *str)
{
	switch (state) {
	case INACTIVE:
		strcpy(str, "INACTIVE");
		break;
	case READY:
		strcpy(str, "READY   ");
		break;
	case RUNNING:
		strcpy(str, "RUNNING ");
		break;
	case WAIT_DLY:
		strcpy(str, "WAIT_DLY");
		break;
	case WAIT_ITV:
		strcpy(str, "WAIT_ITV");
		break;
	case WAIT_OR:
		strcpy(str, "WAIT_OR");
		break;
	case WAIT_AND:
		strcpy(str, "WAIT_AND");
		break;
	case WAIT_SEM:
		strcpy(str, "WAIT_SEM");
		break;
	case WAIT_MBX:
		strcpy(str, "WAIT_MBX");
		break;
	case WAIT_MUT:
		strcpy(str, "WAIT_MUT");
		break;
	case WAIT_MEM:
		strcpy(str, "WAIT_MEM");
		break;
	default:
		strcpy(str, "UNKNOWN");
	}
	return str;
}

/**
 * @brief: get function name by function pointer
 * @param: p the entry point of a function (i.e. function pointer)
 * @param: str the buffer to return the function name
 * @return: the function name string starting address
 */
char *fp2name(void (*p)(), char *str)
{
	int i;
	unsigned char is_found = 0;

	for ( i = 0; i < NUM_FNAMES; i++) {
		if (g_task_map[i].p == p) {
			str = strcpy(str, g_task_map[i].name);
			is_found = 1;
			break;
		}
	}
	if (is_found == 0) {
		strcpy(str, "ghost");
	}
	return str;
}


