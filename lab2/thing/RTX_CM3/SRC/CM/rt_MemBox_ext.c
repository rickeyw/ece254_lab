/*----------------------------------------------------------------------------
 *      ECE254 Lab Task Management
 *----------------------------------------------------------------------------
 *      Name:    RT_MEMBOX_ext.C
 *      Purpose: Interface functions for blocking
 *               fixed memory block management system
 *      Rev.:    V4.60
 *----------------------------------------------------------------------------
 *      This code is extends the RealView Run-Time Library.
 *      Created by University of Waterloo ECE254 Lab Staff.
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *      Includes
 *---------------------------------------------------------------------------*/

#include "rt_TypeDef.h"
//#include "../../INC/RTL_ext.h"
#include "RTX_Config.h"
#include "rt_System.h"
#include "rt_MemBox.h"
#include "rt_HAL_CM.h"
#include "rt_List.h"
#include "rt_Task.h"       /* added in ECE254 lab keil_proc */
#include "rt_Task_ext.h"       /* added in ECE254 lab keil_proc */
#include "rt_MemBox_ext.h" /* added in ECE254 lab keil_proc */

/* ECE254 Lab Comment: You may need to include more header files */

/*----------------------------------------------------------------------------
 *      Global Variables
 *---------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *      Global Functions
 *---------------------------------------------------------------------------*/

/*==========================================================================*/
/*  The following are added for ECE254 Lab Task Management Assignmet       */
/*==========================================================================*/

/*---------------- rt_alloc_box_s, task blocks when out of memory-----------*/

/**
   @brief: Blocking memory allocation routine.
 */
void *rt_alloc_box_s (void *p_mpool) {
  void *free;

  // Try allocation
  free = rt_alloc_box(p_mpool);

  if (free != NULL){ // It worked!
    return free;
  } else { // Blocking...
    //Put this running task to wait list
		if ((&os_mem_wait)->p_lnk != NULL) {
			rt_put_prio(&os_mem_wait, os_tsk.run);
		}
		else {
			(&os_mem_wait)->p_lnk = os_tsk.run;
			os_tsk.run->p_lnk = NULL;
			os_tsk.run->p_rlnk = (P_TCB)(&os_mem_wait);
		}
    
    //Block with no timeout
    rt_block(0xffff, WAIT_MEM);

    return free;
  }
}


/*----------- rt_free_box_s, pair with _s memory allocators ----------------*/
/**
 * @brief: free memory pointed by ptr, it will unblock a task that is waiting
 *         for memory.
 * @return: OS_R_OK on success and OS_R_NOK if ptr does not belong to gp_mpool
 */
OS_RESULT rt_free_box_s (void *p_mpool, void *box) {
  P_TCB task_context;
	
	// If tasks are waiting for memory, transfer the memory to them
  if (os_mem_wait.p_lnk != NULL){
		// Ensure the box is valid
		if (box < p_mpool || box >= ((P_BM) p_mpool)->end) {
			return OS_R_NOK;
		}
		
		// Transfer ownership of the box to the highest priority task waiting for memory
    task_context = rt_get_first(&os_mem_wait);
    task_context->ret_val = (U32) box;
    rt_dispatch(task_context);
		
		return (OS_R_OK);
  }

  // Try freeing memory
  if (rt_free_box(p_mpool,box)){
    return OS_R_NOK;
  } else {
		return (OS_R_OK);
	}
}
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
