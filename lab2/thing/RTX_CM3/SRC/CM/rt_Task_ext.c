/*----------------------------------------------------------------------------
 *      ECE254 Lab Task Management
 *----------------------------------------------------------------------------
 *      Name:    RT_TASK_ext.C
 *      Purpose: Interface functions for extended task management
 *      Rev.:    V4.60
 *----------------------------------------------------------------------------
 *      This code extends the RealView Run-Time Library.
 *      Created by University of Waterloo ECE254 Lab Staff.
 *---------------------------------------------------------------------------*/

#include "rt_TypeDef.h"
#include "RTX_Config.h"
#include "rt_System.h"
#include "rt_Task.h"
#include "rt_List.h"
#include "rt_MemBox.h"
#include "rt_Robin.h"
#include "rt_HAL_CM.h"
#include "rt_Task_ext.h"

/*----------------------------------------------------------------------------
 *      Global Variables
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *      Local Functions
 *---------------------------------------------------------------------------*/
/*--------------------------- rt_tsk_count_get ------------------------------*/
/* added in ECE254 lab keil_rtx */
int rt_tsk_count_get (void) {

	U32 tasks_active = 0;
	P_TCB task_context;
  U32 tid;

	// Same loop as the static OS_TID rt_get_TID (void) to check for tasks in os_active_TCB
  for (tid = 1; tid <= os_maxtaskrun; tid++) {
    if (os_active_TCB[tid-1] != NULL) {
			task_context = os_active_TCB[tid-1];
			if (task_context->state != INACTIVE){
				tasks_active++;
			}
    }
  }

	/*
	* In addition to OS_TASKCNT user tasks, the system creates one system task os_idle_demon.
	* This task is always required by the RTX kernel.
	* Total number of concurrent running tasks is OS_TASKCNT+1 (number of user tasks plus one system task).
	*
	*/
	if (os_idle_TCB_reference != NULL) {
		//	task_context = os_active_TCB[tid];
			if (os_idle_TCB_reference->state != INACTIVE){
				tasks_active++;
			}
  }

	return tasks_active;
}

/*--------------------------- rt_tsk_usage_percent ------------------------------------*/
U8 rt_tsk_usage_percent (P_TCB task_context){
	U32 size, stack_size, stack_used = 0;
	U8 usage_pct;

	//Find out size -- get the high address of our stack
	//size = task_context->priv_stack >> 2; // Dont need to worry about per lab man
	//if( size == 0 ) {
	size = (U16) os_stackinfo >> 2; // size of stack every 4 bytes (words) , because it's already shifted //WORDS. We cast this to U16 because it's U32 const os_stackinfo, and the other bits represent stuff like PRIVATE COUNT which we don't care.
	//}

	// Pointer arithmetic
	stack_size = size << 2; // WORDS
	if(task_context->state == RUNNING){
		stack_used = (U32) &task_context->stack[size] - (U32) rt_get_PSP(); //BYTES
	} else {
		stack_used = (U32) &task_context->stack[size] - (U32) task_context->tsk_stack; //BYTES
	}
	
	//Stack is High to Low (High address is bottom of the stack. As it grows, the address reduces)
	//U32 array to get the pointer. when calculating the pointer address is in byte.

	//running task get from register, non running get from tsk_stack.

	//current stack ptr always point to last value added.

	//Use this to find out percentage
	usage_pct = (stack_used * 100) / stack_size;

	//Stack starts at the stack[size]

	return usage_pct;
}

/*--------------------------- rt_tsk_get ------------------------------------*/
/* added in ECE254 lab keil_proc */
OS_RESULT rt_tsk_get (OS_TID task_id, RL_TASK_INFO *p_task_info) {
	//OS_RESULT in RTL.h line 100 ish
		P_TCB task_context;

	//Error checking
	//Is p_task_info legit?
	if (p_task_info == NULL){
		return OS_R_NOK;
	}
	//Is task legit?
	if (task_id > os_maxtaskrun || task_id < 1){
		return OS_R_NOK;
	}
	// Is task in TCB?
	if (os_active_TCB[task_id-1] != NULL) {
		task_context = os_active_TCB[task_id-1];
	} else {
		return OS_R_NOK;
	}


	//Allocate info
	p_task_info->task_id     = task_context->task_id;
	p_task_info->state       = task_context->state;
	p_task_info->prio        = task_context->prio;
	p_task_info->stack_usage = rt_tsk_usage_percent(task_context);
	p_task_info->ptask       = task_context->ptask;

	return OS_R_OK;
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
