/*----------------------------------------------------------------------------
 *      ECE254 Lab Task Management
 *----------------------------------------------------------------------------
 *      Name:    RT_MEMBOX_EXT.H
 *      Purpose: Interface functions for blocking
 *               fixed memory block management system
 *      Rev.:    V4.60
 *----------------------------------------------------------------------------
 *      This code extends the RealView Run-Time Library.
 *      Created by University of Waterloo ECE254 Lab Staff.
 *---------------------------------------------------------------------------*/
/* Variables */
// // Steal the wait list implementation for ready/sem. wait list
// extern struct OS_XCB os_mem_wait;

/* Functions */
extern void     *rt_alloc_box_s (void *mpool);
extern OS_RESULT rt_free_box_s  (void *mpool, void *ptr);

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
